package kz.aitu.demo.Controller;


import kz.aitu.demo.Entity.Person;
import kz.aitu.demo.Service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(path="/api/v2/users/")
    public List<Person> getAll() {
        return personService.getAll();
    }
    @GetMapping("/api/v2/users/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(personService.getById(id));
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> save(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/v2/users/")
    public ResponseEntity<?> update(@RequestBody Person person ) {
        return ResponseEntity.ok(personService.update(person));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void delete(@PathVariable Long id) {
        personService.delete(id);
    }

}
