package kz.aitu.demo.Service;

import kz.aitu.demo.Entity.Person;
import kz.aitu.demo.Repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }
    public List<Person> getAll() {
        return (List<Person>) personRepository.findAll();
    }

    public Person getById(Long _id) {
        return personRepository.findById(_id).orElse(null);
    }

    public Person create(Person person) {
        return personRepository.save(person);
    }

    public Person  update( Person person) {
        return personRepository.save(person);
    }

    public void delete(Long person_id) {
        personRepository.deleteById(person_id);
    }

}