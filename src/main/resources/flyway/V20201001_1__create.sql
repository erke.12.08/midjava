DROP TABLE IF EXISTS users;

CREATE TABLE users(
	id serial not null
constraint users_pk unique ,
	firstname VARCHAR(50),
	lastname VARCHAR(50),
	city VARCHAR(50),
	phone VARCHAR(50),
	telegram VARCHAR(50)
);

alter table users owner to postgres;

insert into users (id, firstname, lastname, city, phone, telegram) values (1, 'Farr', 'Kornacki', 'Brezovica', '319-381-3330', 'fkornacki0@oracle.com');
insert into users (id, firstname, lastname, city, phone, telegram) values (2, 'Lorettalorna', 'Towe', 'Saint-Paul', '392-521-3614', 'ltowe1@jigsy.com');
insert into users (id, firstname, lastname, city, phone, telegram) values (3, 'Huberto', 'Biggs', 'Wangcungang', '729-631-7500', 'hbiggs2@photobucket.com');
insert into users (id, firstname, lastname, city, phone, telegram) values (4, 'Adah', 'Mc Giffin', 'Pak Phayun', '712-267-2612', 'amcgiffin3@example.com');
insert into users (id, firstname, lastname, city, phone, telegram) values (5, 'Royall', 'Leatt', 'Puro', '978-996-1786', 'rleatt4@barnesandnoble.com');